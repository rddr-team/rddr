.. RDDR documentation master file, created by
   sphinx-quickstart on Fri Jan 10 13:03:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

RDDR Home
================================

.. toctree::
    :maxdepth: 2

    quickstart
    architecture
    config
    py-modindex

Introduction
############

RDDR is an N-Versioning proxy that can identify
bugs in your cloud microservice and prevent them
from being exploited.

RDDR sits in front of N variants of any microservice
and proxies their incoming and outgoing traffic.
It replicates incoming traffic to each instance and then
diffs their responses; it also diffs their outgoing requests and
replicates the response to each of them. If any difference
in behavior is seen among the variants, RDDR will block
the response.

.. figure:: _static/rddr_block_diagram.png
    :width: 400px
    :align: center
    :alt: alternate text
    :figclass: align-center

    RDDR Deployment Block Diagram

RDDR stands for:

:Replicate: Incoming requests are replicated to each variant of the microservice.
:De-noise: RDDR filters random noise from their responses.
:Diff: RDDR diffs the remainder of their responses to detect difference in behavior.
:Respond: If no differences are detected, the merged response is returned to the requester.

Read :doc:`quickstart` to get started using RDDR.

RDDR was developed at the `Spark Research Lab <https://spark.ece.utexas.edu/>`_
at the University of Texas at Austin.


Scope
#####

This documentation will cover the organization of the
RDDR Python module. If time permits, I will also
document the Kubenernetes deployment I have used
for testing. Kubernetes is not required. You can
deploy RDDR outside of a container and point it
at your replicated instances however they are
deployed.


Indices and tables
##################

* :ref:`genindex`
* :ref:`py-modindex`
* :ref:`search`
