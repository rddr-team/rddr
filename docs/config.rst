Configuration
=============

Here is an example config file:

.. code-block:: yaml

    addrlist:
            - 172.17.0.2:443
            - 172.17.0.3:443
            - 172.17.0.4:443
    filter: True
    incoming-proxy:
            host: 0.0.0.0
            port: 4443
            protocol: ssl
            diff-class: "rddr_diff_builtins.RddrHttpDiff"
            enforcing: True
    outgoing-proxies:
            1.2.3.4:3036:
                    host: 0.0.0.0
                    ports:
                            - 3001
                            - 3002
                            - 3003
                    diff-class: "rddr_diff_builtins.RddrJsonDiff"
                    protocol: ssl
                    enforcing: True
            some-hostname:5432:
                    host: 192.168.99.1
                    ports:
                            - 4001
                            - 4002
                            - 4003
                    diff-class: "rddr_diff_builtins.RddrPostgresDiff"
                    diff-params: {
                        ...
                    }
                    protocol: tcp
                    enforcing: True
    verbosity: "DEBUG"

And here is what each of these options configures:

:addrlist:

    Addresses (host:port) of the N application variants we want to proxy.
    If you are doing filtering (i.e. filter is True), make sure the
    addresses of the two instances comprising your filter pair appear
    **first in this list**.

:filter:

    Specify whether or not to filter non-deterministic noise.
    This requires you to deploy two identical application instances.
    This is known as the filter pair.
    **You must list the filter pair instances as the first two entries in addrlist.**

:incoming-proxy:

    Configuration of the single proxy for incoming requests to the N variants

    :host:
        The hostname or IP **which the proxy will bind to**.
        0.0.0.0 indicates it will bind to all available hostnames/IPs.
    :port:
        The port **which the proxy will bind to**.
    :protocol:
        The transport protocol to use for this proxy.
        One of ``tcp``, ``ssl``.
    :diff-class:
        Python class to import for filtering non-deterministic noise.
        This class must implement the functions ``diff_traffic`` and
        ``modify_traffic``.
        RDDR ships with four such classes:
        :py:class:`rddr_diff_builtins.RddrHttpDiff`,
        :py:class:`rddr_diff_builtins.RddrJsonDiff`,
        :py:class:`rddr_diff_builtins.RddrByteDiff`, and
        :py:class:`rddr_diff_builtins.RddrPostgresDiff`,
    :enforcing:
        When the N variants' responses to a request differ (after filtering
        non-deterministic noise), ``enforcing`` determines whether or not
        the server will forward the merged response to the client.
        ``enforcing = True`` will block the response.

:outgoing-proxies:
    Configure one outgoing proxy per backend service
    that the N variants make requests to.

    :<dest 1>:
        This proxy will forward traffic to this destination.
        Specify the destination as in ``addrlist``: host:port

        :host:
            The hostname or IP **which the proxy will bind to**.
            0.0.0.0 indicates it will bind to all available hostnames/IPs.
        :ports:
            The list of ports **which the proxy will bind to**.
            Each port should accept connections from one of the N instances.
            The first port in the list corresponds to the first instance in addrlist,
            the second port with the second instance, and so on.
            Update the N variants to send their requests to the
            bound host and the port associated with them.
            Proxy will bind to <host>:<port_1>, ..., <host>:<port_N>.
        :protocol:
            The transport protocol to use for this proxy.
            One of ``tcp``, ``ssl``.
        :diff-class:
            Python class to import for filtering non-deterministic noise.
        :diff-params:
            Some RDDR diff plugins accept a set of configuration parameters.
            The plugin will specify its own schema for this field.
        :enforcing:
            If the N variants differ in their outgoing requests
            (after filtering non-deterministic noise),
            ``enforcing`` determines whether or not the server
            will forward the merged response to the client.
            ``enforcing = True`` will block the request.

    ...

:verbosity: Verbosity level.
            One of ``INFO``, ``DEBUG``, ``WARNING``, ``ERROR``.

This RDDR deployment can be visualized as:

.. figure:: _static/config-diagram.png
    :width: 400px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Block diagram of the above configuration
