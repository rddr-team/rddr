
Quick Start
===========

This page will get you up and running with
RDDR in no time. We will deploy 3 instances
of DVWA running at different security levels
behind RDDR sharing a single database microservice.

.. figure:: _static/dvwa_block_diagram.png
    :width: 375px
    :align: center
    :alt: alternate text
    :figclass: align-center

    DVWA deployment with RDDR block diagram

1. `Install Kubernetes on your system. <https://kubernetes.io/docs/setup/>`_
and enable the DNS service. Istio is not supported at this time; we've
seen issues with Istio sidecars. Best to disable sidecars for now.

2. Clone the RDDR project repository:

.. code-block:: bash

    git clone https://rjw245@bitbucket.org/rjw245/rddr.git

3. Move to the folder rddr/deployments/dvwa_frontend/k8s

.. code-block:: bash

    cd rddr/deployments/dvwa_frontend/k8s

4. Apply all kubernetes yaml files in this directory.
The command for microk8s is shown below:

.. code-block:: bash

    microk8s.kubectl apply -f .

Wait until all pods are in the Running state. MySQL can take some time
even after it's in the Running state to become fully ready.

5. Open localhost:31001 in your browser. You should see the login page:

.. figure:: _static/dvwa_login.png
    :width: 300px
    :align: center
    :alt: alternate text
    :figclass: align-center

    DVWA login page

6. Login with user ``admin``, password ``password``.

7. Click "Create/Reset Database" and login once more.

8. Navigate to the "SQL Injection" tab.
/home/riley/rddr/docs/.venv/bin/pygmentize
9. Enter a benign input in the User ID box, such as ``1``.
Press enter and you'll see the request goes through.

.. figure:: _static/dvwa_sqli_benign.png
    :width: 500px
    :align: center
    :alt: alternate text
    :figclass: align-center

    DVWA SQL Injection with benign input

10. Enter a malicious input such as:

.. code-block:: text

    ' UNION SELECT user,password from users WHERE '1'='1

and press enter.

.. figure:: _static/dvwa_sqli_malicious.png
    :width: 500px
    :align: center
    :alt: alternate text
    :figclass: align-center

    DVWA SQL Injection with malicious input

11. You should see the request gets denied by RDDR:

.. figure:: _static/rddr_denial.png
    :width: 300px
    :align: center
    :alt: alternate text
    :figclass: align-center

    RDDR Denying Access

This is because we've deployed multiple instances of DVWA
configured for different security levels. The more secure
instance properly sanitizes the SQL query which causes its
query to the database to look different from that of the
other instances. RDDR catches this divergence in behavior
and blocks the response to the user so that the SQL sanitization
bug cannot be exploited.
