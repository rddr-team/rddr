
Architecture
============

Here I will describe how RDDR is structured
and link to some of the classes that make it up.


Overview
########

.. figure:: _static/rddr_block_diagram.png
    :width: 500px
    :align: center
    :alt: alternate text
    :figclass: align-center

    RDDR Deployment Block Diagram

Above is a simplified view of an RDDR deployment.
RDDR sits on either side of a set of instances of
the same application. Incoming requests to the proxy
are replicated to all application instances, and
their responses are diffed and merged before being sent
back. Any outgoing requests from the application instances
pass through an outgoing proxy which diffs and merges the
requests before passing the request on to the target microservice.
The response back is then replicated to all app instances.
If the application relies on multiple separate backend services,
an outgoing request proxy should be created for each.
The entire RDDR deployment is placed behind a production-grade
Envoy proxy.

RDDR shines when the application instances differ from one another.
Subtle variations in the application that do not change
nominal behavior can help to catch bugs and prevent exploitation
of them.

An ideal deployment will also consist of two copies of the application
that are identical. These form the "filter pair" which helps
RDDR to distinguish between random noise and real bugs.
Because the filter pair are known to be identical, any differences
in their behavior will be ignored across the entire set.
See `Filtering Non-Deterministic Noise`_ for more information.


Running RDDR
############

RDDR is packaged as a Python module with a main method.

.. automodule:: rddr.__main__
   :members:
   :private-members:
   :show-inheritance:

It should be executed like so once installed:

.. code-block:: bash

    python -m rddr [-c path/to/config.yaml]

RDDR accepts the following arguments:

.. code-block:: text

    usage: python -m rddr [-h] [-c CONFIG]

    optional arguments:
    -h, --help            show this help message and exit
    -c CONFIG, --config CONFIG

The main method parses the config file
and starts the top level RDDR class, shown below:

.. autoclass:: rddr.rddr.Rddr
   :members:
   :show-inheritance:


Proxies
#######

RDDR implements separate proxies for incoming versus outgoing requests.
Both of these proxy classes share the
:class:`RddrProxy <rddr.proxies.proxy.RddrProxy>` parent class.

.. autoclass:: rddr.proxies.proxy.RddrProxy
   :members:
   :show-inheritance:

Incoming Proxy
--------------
The :class:`RddrIncomingProxy <rddr.proxies.incoming_proxy.RddrIncomingProxy>`
class implements one proxy between a client and N variants
of a server.

.. autoclass:: rddr.proxies.incoming_proxy.RddrIncomingProxy
   :members:
   :show-inheritance:

Outgoing Proxy
--------------
The :class:`RddrOutgoingProxy <rddr.proxies.outgoing_proxy.RddrOutgoingProxy>`
class implements one proxy between N variants of an application
and one server they want to query.

.. autoclass:: rddr.proxies.outgoing_proxy.RddrOutgoingProxy
   :members:
   :show-inheritance:


Support for Different Transport Protocols
#########################################

We intend RDDR to support a variety of transport protocols.
The latest version of RDDR supports:

- Unencrypted TCP
- SSL

Each proxy (incoming and outgoing) can be configured for a different
transport protocol. This is useful in the cloud, where applications
can be stitched from many smaller microservices that all speak
different protocols. See :doc:`config` for more on configuring the
protocol.

.. autoclass:: rddr.protocols.protocol.RddrProtocol
   :members:
   :show-inheritance:

TCP
---

.. autoclass:: rddr.protocols.protocol_tcp.RddrProtocolTcp
   :members:
   :show-inheritance:

SSL
---

.. autoclass:: rddr.protocols.protocol_ssl.RddrProtocolSsl
   :members:
   :show-inheritance:


Filtering Non-Deterministic Noise
#################################

You can deploy two identical app instances which will help to
filter any non-deterministic noise in the system.
See the ``filter`` parameter in :doc:`config`.

Consider a query that fetches a random number from the application.
Every instance will generate a different random number.
Without filtering, the incoming proxy would flag this divergence
as a potential bug. However, if the proxy
sees that the two identical apps also differ in their responses,
we can safely ignore this region of the response and in
doing so we've filtered out the non-deterministic noise.


Support for Diffing Various Application Data
############################################

Filtering non-deterministic noise as described above
requires the proxies to do a minimal amount of parsing
of the data. For example, if the data is in JSON format,
we may want ignore certain non-deterministic keys of
the data structure. For a text file, we may instead
want to ignore certain lines. We need a way to
tokenize the data being transferred so that we can
ignore particular tokens. Since the tokenizing algorithm
is likely to vary across applications, we have defined
a simple interface for others to extend.

Interface Specification
-----------------------

Simply implement a class that inherits from
:class:`AbstractRddrDiff <rddr.AbstractRddrDiff>`
and implement the functions ``diff_traffic``,
``modify_traffic``, and optionally ``render_denial``
and ``validate_params``:

.. autoclass:: rddr.AbstractRddrDiff
   :members:
   :show-inheritance:

We have packaged four classes which implement the interface
for JSON, HTTP, raw bytes, and Postgres respectively.
These are shown below:

JSON
----

.. autoclass:: rddr_diff_builtins.RddrJsonDiff
   :members:
   :undoc-members:
   :show-inheritance:

HTTP
----
.. autoclass:: rddr_diff_builtins.RddrHttpDiff
   :members:
   :undoc-members:
   :show-inheritance:

Bytewise
--------
.. autoclass:: rddr_diff_builtins.RddrByteDiff
   :members:
   :undoc-members:
   :show-inheritance:

PostgreSQL
--------
.. autoclass:: rddr_diff_builtins.RddrPostgresDiff
   :members:
   :undoc-members:
   :show-inheritance:

These classes each implement the functions
``diff_traffic`` and ``modify_traffic``.

Users may write their own classes that implement
the above functions to tailor RDDR's filtering engine
for their own application. Simply include the name
of your class via the ``diff-class`` field of your config file.
