import BaseHTTPServer, SimpleHTTPServer
import ssl
import sys
import os

certfile = os.path.join(os.path.dirname(os.path.realpath(__file__)),'server.pem')
port = int(sys.argv[1])
httpd = BaseHTTPServer.HTTPServer(('0.0.0.0',port), SimpleHTTPServer.SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket (httpd.socket,  certfile=certfile, server_side=True)
httpd.serve_forever()

