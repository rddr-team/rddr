from flask import Flask, Response
import random
import sys
import json
import argparse
import os

app = Flask(__name__)

divergent = False

@app.route("/secret")
def secret():
    global divergent
    content = {"field1": "Some data", "field2": "More data"}
    if divergent:
        content["field2"] = "Divergent behavior!"
    return Response(json.dumps(content), mimetype="application/json")

@app.route("/")
def hello():
    content = {"static": "ABCDEFG",
               "dynamic": int.from_bytes(os.urandom(4), byteorder='big')}
    return Response(json.dumps(content), mimetype="application/json")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("port")
    parser.add_argument("--divergent", action="store_true")
    args = parser.parse_args()
    divergent = args.divergent
    app.run(host='0.0.0.0', port=int(args.port), ssl_context='adhoc')

