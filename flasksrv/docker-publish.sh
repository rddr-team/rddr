#!/bin/bash

docker build -t flasksrv:latest .
docker tag flasksrv localhost:32000/flasksrv
docker push localhost:32000/flasksrv

docker tag flasksrv utspark/flasksrv
docker push utspark/flasksrv
