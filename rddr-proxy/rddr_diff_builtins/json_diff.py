import jsondiff
import json
import itertools
import pkg_resources
import logging
from logging import Logger
from typing import Dict, Optional, List, Tuple

from parse_http_resp import parse_http_response
from rddr import AbstractRddrDiff

__default_logger = logging.getLogger("rddr.json_diff")

class RddrJsonDiff(AbstractRddrDiff):
    """
    Diff tool for JSON documents that ships with RDDR.
    JSON is expected to be embedded in an HTTP response.
    Differences key by key.
    Does not modify incoming traffic.
    """
    def _default_logger_name(self) -> str:
        return "rddr.json_diff"

    def diff_traffic(self, traffic: List[bytes]) -> List[Tuple[int, bool]]:
        """
        Parses JSON documents embedded in HTTP responses.
        May request more bytes of a given stream if a partial
        JSON document has been received and cannot yet be parsed.
        Differences key by key.

        See interface definition :meth:`rddr.AbstractRddrDiff.diff_traffic` for more.

        :param traffic: List of traffic from app instances.
        """
        #if determiniistic get the differences we can ignore first
        nbytes = [len(t) for t in traffic]
        if all([len(r)==0 for r in traffic]):
            return zip(nbytes, [False]*len(nbytes))
        if self.do_filter:
            return self._filter_and_diff(traffic)
        #if not deterministic just diff and return
        else:
            raise NotImplementedError("JSON diffing without filter pair not implemented.")


    def _extract_json_from_http(self, http_data):
        headers, sep, json_string = parse_http_response(http_data).partition(b'\r\n\r\n')
        return json.loads(json_string)


    def _get_diff(self, traffic):
        # get the traffic for the filter pairs
        ps = traffic[:2]
        # get the json out
        rs = [self._extract_json_from_http(x) for x in ps]

        #can delete one of them cause we already checked it
        del traffic[0]
        # return the difference
        return jsondiff.diff(rs[0],rs[1])


    def _filter_and_diff(self, traffic):
        num_srcs = len(traffic)
        nbytes = [len(t) for t in traffic]
        try:
            js = self._get_diff(traffic)
        except json.JSONDecodeError:
            self.logger.warning("JSON decode error")
            self.logger.debug(traffic)
            return [(0, 0<nb) for nb in nbytes]

        #get all the keys that are dynamic
        dks = js.keys()

        # Read in all the traffic
        # rs is a list of dictionaries
        rs = []
        for r in traffic:
            try:
                r = self._extract_json_from_http(r)
            except json.JSONDecodeError:
                self.logger.warning("JSON decode error")
                self.logger.debug(traffic)
                return [(0, 0<nb) for nb in nbytes]
            rs.append(r)

        # first dictionary of json
        bd = rs[0]
        dk = list(bd.keys())
        dk.sort()

        #run loop on the rest of the json dictionaries
        for e in rs[1:]:
            ck = list(e.keys())
            ck.sort()
            #make sure they have all the same keys
            if dk != ck:
                self.logger.warning(f"Keys differ: {dk} vs {ck}")
                return [(-1, False)]*num_srcs
            #go through each element
            for k in e:
                if k in dks:
                    self.logger.debug("dynamic content skip")
                    continue
                else:
                    tv = e[k]
                    if tv != bd[k]:
                        self.logger.warning(f"Values differ: {tv} vs {bd[k]}")
                        return [(-1, False)]*num_srcs
        return zip(nbytes, [False]*len(nbytes))

    def render_denial(self) -> bytes:
        """
        Returns an HTTP response string containing a 500 error and
        an "access denied" message, with the RDDR logo.
        See static/denied.html for the content.
        """
        page = (b"HTTP/1.1 500 Internal Server Error\r\n"
                b"Content-Type: text/html\r\n\r\n")
        with open(pkg_resources.resource_filename('rddr.proxies', 'static/denied.html'), 'rb') as f:
            page += f.read()
        return page
