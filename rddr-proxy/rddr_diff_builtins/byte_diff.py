import logging
from logging import Logger
from typing import Dict, Optional, List, Tuple
from rddr import AbstractRddrDiff

class RddrByteDiff(AbstractRddrDiff):
    def _default_logger_name(self) -> str:
        return "rddr.byte_diff"

    def diff_traffic(self, traffic: List[bytes]) -> List[Tuple[int, bool]]:
        """
        Validates that messages match byte for byte.

        See interface definition :meth:`rddr.AbstractRddrDiff.diff_traffic` for more.

        :param traffic: List of traffic from app instances.
                        Key = instance address "host:port"
                        Value = Bytes response
        """
        nbytes = [len(t) for t in traffic]
        consistent = True
        filtered_byte_idxs = []
        baseline = traffic[0]
        for i, byte in enumerate(baseline):
            byte_consistent = True
            for resp in traffic:
                byte_consistent = byte_consistent and i < len(resp) and resp[i] == byte
            if self.do_filter and (i>=len(traffic[1]) or traffic[1][i] != byte):
                filtered_byte_idxs.append(i)
            else:
                consistent = consistent and byte_consistent

        if self.do_filter and len(filtered_byte_idxs) > 0:
            self.logger.debug("Filtered bytes")
            for inst_i, resp in enumerate(traffic):
                resp_repr = ""
                for i, byte in enumerate(resp):
                    byte_repr = chr(byte) if chr(byte).isprintable() else "x{:02X}".format(byte)
                    if i in filtered_byte_idxs:
                        byte_repr = '\033[91m' + byte_repr + '\033[0m'
                    resp_repr += byte_repr
                self.logger.debug(f"Instance {inst_i} sent {resp_repr}")

        if not consistent:
            self.logger.debug("Divergence!")
            for inst_i, data in enumerate(traffic):
                self.logger.debug(f"Instance {inst_i} sent {data}")
            return [(-1, False)]*len(traffic)
        else:
            return zip(nbytes, [False]*len(nbytes))
