import logging
from logging import Logger
from typing import Dict, Optional, List, Tuple
from itertools import zip_longest
import struct
import cerberus

from rddr import AbstractRddrDiff

_pgsql_param_schema = {
    "tokens": {
        "type": "list",
        "schema": {
            "type": "list",
            "schema": {
                "type": "binary",
                "coerce": lambda s: bytes(s, encoding='utf8'),
            },
        },
        "required": False,
        "nullable": True,
        "default": None,
    },
}

class RddrPostgresDiff(AbstractRddrDiff):
    """
    This class enables support for diffing Postgres
    traffic across N application instances.
    This diff plugin supports ``diff-params``.
    ``diff-params`` should be a dictionary with one
    key: ``tokens``.
    ``tokens`` is a list of lists of bytestrings,
    one bytestring per application instance. This
    allows you to preconfigure tokens you expect to
    be different among the Postgres instances. An
    example is the string reported for the server
    version -- different variants will provide
    different strings. By specifying that here,
    you can avoid flagging that as divergent behavior.
    """
    _backend_pkt_types = (
        b'1', # ParseComplete
        b'2', # BindComplete
        b'3', # CloseComplete
        b'A', # NotificationResponse
        b'C', # CommandComplete
        b'c', # CopyDone
        b'd', # CopyData
        b'D', # DataRow
        b'E', # ErrorResponse
        b'G', # CopyInResponse
        b'H', # CopyOutResponse
        b'I', # EmptyQueryResponse
        b'K', # BackendKeyData
        b'n', # NoData
        b'N', # NoticeResponse
        b'R', # Authentication*
        b'S', # ParameterStatus
        b's', # PortalSuspended
        b't', # ParameterDescription
        b'T', # RowDescription
        b'V', # FunctionCallResponse
        b'Z', # ReadyForQuery
    )
    _backend_pkt_types_to_ignore = (
        b'C', # CommandComplete
        b'K', # BackendKeyData
        b'S', # ParameterStatus
        b'T', # RowDescription
    )

    _frontend_pkt_types = (
        b'B', # Bind
        b'C', # Close
        b'c', # CopyDone
        b'd', # CopyData
        b'D', # Describe
        b'E', # Execute
        b'f', # CopyFail
        b'F', # FunctionCall
        b'H', # Flush
        b'P', # Parse
        b'p', # PasswordMessage
        b'Q', # Query
        b'S', # Sync
        b'X', # Terminate
    )

    def _default_logger_name(self) -> str:
        return "rddr.pgsql_diff"

    def _pgsql_backend_pkts(self, data: bytes, inst_i: int) -> List[Tuple[bytes, bytes]]:
        """
        Generator. Yields packets from a bytestring of data.
        Packets are a 2-tuple of the form (type, data).
        Both type and data are bytestrings.

        Raises an exception if an unknown packet type is received.

        :param data: Bytestring of postgres traffic to parse.
        """
        bytes_parsed = 0
        while True:
            orig_data = data
            orig_data_len = len(data)
            if len(data) == 0:
                break
            packet_type = data[0:1]
            bytes_parsed += 1
            data = data[1:]
            if len(data) == 0 and packet_type in (b'S', b'N'):
                # Special case, responding to SSL request. Single byte response.
                self.logger.debug(f"SSL response.")
                yield (b'ssl:'+packet_type, b'', bytes_parsed)
            elif packet_type in RddrPostgresDiff._backend_pkt_types:
                # Packet len includes the 4 byte of this length specifier
                # hence subtract 4
                if len(data) < 4:
                    self.logger.debug(f"Insufficient data to unpack packet length from inst {inst_i}")
                    break # Partial packet
                packet_len = struct.unpack("!L", data[:4])[0]-4
                bytes_parsed += 4
                data = data[4:]
                packet_data = data[:packet_len]
                if len(packet_data) != packet_len:
                    self.logger.debug(f"Insufficient data. Expecting payload of {packet_len} bytes, got {len(packet_data)}")
                    break # Partial packet
                bytes_parsed += len(packet_data)
                data = data[packet_len:]
                yield (packet_type, packet_data, bytes_parsed)
            else:
                self.logger.debug(f"Data: {orig_data} len {orig_data_len} from inst {inst_i}")
                raise Exception(f"Unknown packet type: {packet_type}")

    def diff_traffic(self, traffic: List[bytes]) -> List[Tuple[int, bool]]:
        """
        Validates that Postgres messages match.
        Ignores certain packet types.
        See member _backend_pkt_types_to_ignore for the full list of ignored packet types.
        Prior to diffing, will substitute tokens preconfigured in the config file
        under the ``diff-params`` key for the associated proxy.

        See interface definition :meth:`rddr.AbstractRddrDiff.diff_traffic` for more.

        :param traffic: List of traffic from app instances.
        """
        consistent = True
        bytes_parsed_of_each = [0]*len(traffic)
        need_more_bytes=[False]*len(traffic)
        for packets in zip_longest(*[self._pgsql_backend_pkts(d, i) for i, d in enumerate(traffic)]):
            do_diff = True
            for i, p in enumerate(packets):
                self.logger.debug(f"Packet: {p}")
                if p is None:
                    need_more_bytes[i] = True
                    do_diff = False
            if do_diff:
                # Substitute preconfigured tokens
                for tokset in self.params["tokens"]:
                    packets = [(p[0], p[1].replace(tokset[i], tokset[0]), p[2]) for i, p in enumerate(packets)]
                p0type = packets[0][0]
                p0data = packets[0][1]
                bytes_parsed_of_each = [p[2] for p in packets]
                # If packet types we care about differ from one another...
                if any([(ptype != p0type) and (ptype not in RddrPostgresDiff._backend_pkt_types_to_ignore) for ptype, _, _ in packets]):
                    if self.do_filter and (packets[0][0] != packets[1][0]):
                        self.logger.debug("Filtering packet type mismatch between filter pair.")
                    else:
                        self.logger.warning(f"Divergence! Postgres packet type mismatch. {[ptype for ptype, _, _ in packets]}")
                        consistent = False
                        break
                if p0type not in RddrPostgresDiff._backend_pkt_types_to_ignore:
                    if any([pdata != p0data for _, pdata, _ in packets]):
                        if self.do_filter and (packets[0][1] != packets[1][1]):
                            self.logger.debug("Filtering packet data mismatch between filter pair.")
                        else:
                            self.logger.warning(f"Divergence! Postgres packet data mismatch on packet type {p0type}.")
                            consistent = False
                            break
            elif need_more_bytes.count(True)>=1:
                break

        if any([bp > 0 for bp in bytes_parsed_of_each]):
            self.logger.debug(f"Bytes parsed: {bytes_parsed_of_each} of {[len(t) for t in traffic]}")

        if consistent:
            return zip(bytes_parsed_of_each, need_more_bytes)
        else:
            return [(-1, False)]*len(traffic)

    def validate_params(self):
        """
        Validates the ``diff-params`` config field for this particular class.
        """
        v = cerberus.Validator(_pgsql_param_schema)
        if not v.validate(self.params):
            print(v.errors)
            raise Exception("Diff params does not comply with schema")
        self.params = v.normalized(self.params)

        if self.params["tokens"] is None:
            self.params["tokens"] = []

    def render_denial(self) -> bytes:
        """
        The diff interface can implement a custom error message
        appropriate for the application layer protocol being handled.
        An error message, for example.
        Default implementation returns empty byte string.

        :return: Bytestring to be sent back to
                 the client if divergent behavior is seen.
        """
        return struct.pack("!cLx", b'E', 1)
