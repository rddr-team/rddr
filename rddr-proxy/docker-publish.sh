#!/bin/bash

docker build -t rddr:latest .

# Push to kubernetes registry
docker tag rddr localhost:32000/rddr
docker push localhost:32000/rddr

# Push to Docker hub
docker tag rddr utspark/rddr
docker push utspark/rddr

