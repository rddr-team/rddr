import logging
import asyncio
import traceback
from multiprocessing import Manager
from concurrent.futures import ThreadPoolExecutor

from rddr.proxies import RddrIncomingProxy, RddrOutgoingProxy

class Rddr(object):
    """
    Top level class for Rddr.
    Encapsulates one incoming proxy and one or more
    outgoing proxy.
    
    :param config: RDDR configuration dictionary
    """
    def __init__(self, config: dict):
        self.mp_manager = Manager()
        self.rip = RddrIncomingProxy(self.mp_manager, config)
        self.rops = []
        # Set up outgoing proxy servers
        if config['outgoing-proxies'] is not None:
            for dest in config['outgoing-proxies'].keys():
                rop = RddrOutgoingProxy(self.mp_manager, config, dest)
                self.rops.append(rop)
        self.logger = logging.getLogger("rddr")

    def run(self):
        """
        Endless loop.
        Calls run() on all proxies configured in separate threads.
        """
        self.rip.init_server()
        for rop in self.rops:
            rop.init_server()
        workers = 0
        for rop in self.rops:
            workers += len(rop.servers)
        workers += 1
        with ThreadPoolExecutor(max_workers=workers) as executor:
            for rop in self.rops:
                for server in rop.servers:
                    executor.submit(server.serve_forever)
            executor.submit(self.rip.server.serve_forever)
        while True:
            pass
        # proxy_cortns = [self.rip.run()]
        # for rop in self.rops:
        #     await rop.init_server()
        #     proxy_cortns.append(rop.run())
        # done, pending = await asyncio.wait(
        #     proxy_cortns,
        #     return_when=asyncio.FIRST_EXCEPTION
        # )
        # for item in done:
        #     if item.exception():
        #         raise item.exception()

    def handle_exception(self, loop, context):
        # context["message"] will always be there; but context["exception"] may not
        msg = context.get("exception", context["message"])
        self.logger.error(f"Caught exception: {msg}")
        traceback.print_exc()
