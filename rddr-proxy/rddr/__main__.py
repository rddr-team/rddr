import yaml
import argparse
import logging
import sys
import asyncio
from multiprocessing import Manager

from rddr.config import validate_cfg
from rddr.rddr import Rddr

def __install_trace_log_level():
    logging.TRACE = logging.DEBUG - 5
    def _trace_root(message, *args, **kwargs):
        logging.log(logging.TRACE, message, *args, **kwargs)
    def _trace_logger(self, message, *args, **kwargs):
        if self.isEnabledFor(logging.TRACE):
            self._log(logging.TRACE, message, *args, **kwargs)
    logging.trace =  _trace_root
    logging.addLevelName(logging.TRACE, "TRACE")
    logging.getLoggerClass().trace = _trace_logger

def main():
    """
    Entrypoint for the RDDR proxy.
    Parses the config file and starts RDDR.
    """
    parser = argparse.ArgumentParser(prog='python -m rddr')
    parser.add_argument("-c", "--config", default="./rddr-config.yaml")
    args = parser.parse_args()

    #open and read the config file
    with open(args.config) as f:
        config = yaml.load(f.read(), Loader=yaml.SafeLoader)

    # Inserts default values, normalizes config file according to schema
    # Raises exception if invalid.
    config = validate_cfg(config)

    __install_trace_log_level()

    logFilter = logging.Filter("rddr")
    logFormatter = logging.Formatter("%(name)s - %(levelname)s: %(message)s")
    logHandler = logging.StreamHandler()
    logHandler.setFormatter(logFormatter)
    logHandler.addFilter(logFilter)
    logging.getLogger().addHandler(logHandler)
    logLevel = eval("logging."+config['verbosity'])
    logging.getLogger().setLevel(logLevel)

    # logging.getLogger("rddr.proxy.incoming").setLevel(logging.WARNING)

    rddr = Rddr(config)

    if config['profile-output'] is not None:
        import yappi
        yappi.start()

    asyncio.get_event_loop().set_exception_handler(rddr.handle_exception)

    try:
        rddr.run()
    except:
        if config['profile-output'] is not None:
            yappi.get_func_stats().save(config['profile-output'], type='pstat')
            logging.info("Saved profiling data")
        raise

if __name__ == "__main__":
    main()
