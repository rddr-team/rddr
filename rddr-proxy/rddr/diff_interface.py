from abc import ABC, abstractmethod
from typing import List, Tuple, Optional
import logging
from logging import Logger

class AbstractRddrDiff(ABC):
    """
    Defines the interface for all RDDR diff plugins.
    Users may extend this class to add support for
    a particular protocol to RDDR.
    Diff plugins may optionally specify configuration
    parameters that a user may provide through the config YAML file.
    The `diff-params` key of the YAML file is reserved
    on each proxy for use by the diff plugin applied to that proxy.
    The schema expected by the plugin should be well-specified.
    Diff plugins should implement `validate_params` to validate
    the schema of the user-provided `diff-params`.

    :param do_filter: If True, will use the first two traffic streams
                        as a filter pair to filter out non-deterministic
                        noise.
    :param logger: The logger instance to use for printing messages.
    :param params: Miscellaneous user-provided config for the plugin,
                   from the user's YAML config file.
                   Subclasses should define clearly what they expect
                   to be passed as parameters.
    """
    def __init__(self,
                 mp_manager,
                 shared_state,
                 do_filter: bool = False,
                 logger: Optional[Logger] = None,
                 params: Optional[dict] = None):
        self.mp_manager = mp_manager
        self.shared_state = shared_state
        self.do_filter = do_filter
        if logger is None:
            self.logger = logging.getLogger(self._default_logger_name())
        else:
            self.logger = logger
        self.params = params if params is not None else {}
        self.validate_params()

    def __new__(cls,
                mp_manager,
                shared_state,
                do_filter: bool = False,
                logger: Optional[Logger] = None,
                params: Optional[dict] = None):
        if cls is AbstractRddrDiff:
            raise TypeError("Can't instantiate abstract class")
        return super(AbstractRddrDiff, cls).__new__(cls)

    def diff_traffic(self, traffic: List[bytes]) -> List[Tuple[int, bool]]:
        """
        Diffs the traffic from N instances.
        Also indicates how many bytes of each traffic stream
        has been processed and whether or not more bytes
        of the stream are needed to process it.
        This default implementation will never detect divergence,
        always processes the entire stream and never requests more bytes.
        Subclasses may raise the RddrInsufficientData exception
        if diff_traffic was called on partial data (i.e. more data
        is required from the instances to make a decision).
        The proxy tunnel will handle this exception by reading
        from the instances once more before calling diff_traffic again.

        :param traffic: List of bytestrings from N instances.

        :return: A list of 2-tuples, one tuple for each traffic stream
                 provided through the "traffic" argument.
                 Each tuple is of the form (int, bool). The first element
                 of the tuple is the number of bytes of that stream that have
                 been differenced and can safely be sent along to the client.
                 If this value is zero, no bytes have yet been parsed.
                 If this value is less than zero, then the streams differ from
                 one another, and the traffic SHOULD NOT be forward to the client.
                 The second element of the tuple is a flag indicating whether
                 or not more bytes are required from the traffic source in order
                 to parse this stream. This is useful if the plugin tokenizes
                 the streams and has to this point received a partial token
                 and requires more bytes to fully difference everything.
        """
        return [(len(t), False) for t in traffic]

    def modify_traffic(self, traffic: bytes, n_instances: int) -> List[bytes]:
        """
        This function replicates one incoming stream into N for each of the
        N application variants. In the process, it may make modifications
        to the replica for each instance as necessary. This can be necessary
        if there are unique tokens that need to be substituted for each instance,
        as in the case of CSRF tokens in HTML forms.
        This default implementation makes no modifications to the traffic.

        :param traffic: Request to modify per recipient in addrlist.
        :param n_instances: Number of app instances in this deployment
        :return: List of the traffic to send to each of the app variants.
        """
        return [traffic]*n_instances

    def render_denial(self) -> bytes:
        """
        The diff interface can implement a custom error message
        appropriate for the application layer protocol being handled.
        An error message, for example.
        Default implementation returns empty byte string.

        :return: Bytestring to be sent back to
                 the client if divergent behavior is seen.
        """
        return b""
    
    def _default_logger_name(self) -> str:
        """
        The string name to assign to the logger by default.
        """
        return "rddr." + self.__class__.__name__

    def validate_params(self):
        """
        Validates the ``diff-params`` key in the user config
        file. By default, does nothing.
        """
        pass
