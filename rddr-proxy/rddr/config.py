import cerberus
import logging
import socket

def proxy_schema(direction):
    base_proxy_schema = {
        "host": {
            "type": "string",
            "required": False,
            "default": "localhost"
        },
        "protocol": {
            "type": "string",
            "required": False,
            "default": "ssl",
            "allowed": [
                "ssl",
                "tcp",
                "ssh",
            ],
        },
        "enforcing": {
            "type": "boolean",
            "required": False,
            "default": True
        },
        "diff-class": {
            "type" : "string",
            "required": False,
            "default" : "rddr_diff_builtins.RddrByteDiff"
        },
        "diff-params": {
            "required": False,
            "nullable": True,
            "default": None,
        },
        "timeout": {
            "type": "float",
            "required": False,
            "default": 30,
        }
    }

    if direction=='outgoing':
        base_proxy_schema['ports'] = {
            "type": "list",
            "required": True,
            "schema": {
                "type": "integer",
            },
        }
    elif direction=='incoming':
        base_proxy_schema['port'] = {
            "type": "integer",
            "required": True,
        }
    else:
        raise ValueError("Invalid proxy direction")

    return base_proxy_schema


_config_schema = {
    "addrlist": {
        "type": "list",
        "required": True,
        "schema": {
            "type": "string"
        }
    },
    "filter": {
        "type": "boolean",
        "required": False,
        "default": False,
    },
    "incoming-proxy": {
        "type": "dict",
        "schema": proxy_schema('incoming'),
    },
    "outgoing-proxies": {
        "type": "dict",
        "required": False,
        "nullable": True,
        "default": None,
        "valuesrules": {
            "type": "dict",
            "schema": proxy_schema('outgoing'),
        },
    },
    "verbosity": {
        "type": "string",
        "required": False,
        "default": "INFO",
        "allowed": list(logging._levelToName.values())+["TRACE"],
    },
    "profile-output": {
        "type": "string",
        "required": False,
        "nullable": True,
        "default": None,
    }
}

def _resolve_hostname(addr: str):
    addr = addr.split(':')
    return ':'.join([socket.gethostbyname(addr[0]), addr[1]])

def validate_cfg(config: dict):
    """
    Uses the cerberus module to validate the user's configuration.
    Ensures config is valid, conforms to schema
    (given by variable ``_proxy_schema`` defined in rddr.config)

    :param config: Dictionary of config parsed from the yaml file.
    """
    v = cerberus.Validator(_config_schema)
    if not v.validate(config):
        print(v.errors)
        raise Exception("Config does not comply with schema")
    config = v.normalized(config)

    if config['outgoing-proxies'] is not None:
        for outproxy in config['outgoing-proxies'].values():
            if len(set(outproxy['ports'])) != len(config['addrlist']):
                raise Exception("You must specify a unique port per instance in the outgoing proxy.")

    config['addrlist'] = [_resolve_hostname(a) for a in config['addrlist']]

    return config
