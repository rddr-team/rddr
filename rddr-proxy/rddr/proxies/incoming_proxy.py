#! /usr/bin/env python3
# Requires at least Python 3.6.8

import socket
import select
import logging
import time
import asyncio
from multiprocessing import Process, reduction, Pipe
import traceback

from .proxy import RddrProxy
from .proxy_server import RddrProxyServer


class RddrIncomingProxy(RddrProxy):
    """
    Implements an incoming proxy for RDDR.
    Replicates incoming requests to N applications
    and diffs their responses before forwarding
    their response.

    :param config: Dictionary of user config provided to RDDR at the command line
    """
    def __init__(self, mp_manager, config: dict):
        super().__init__(mp_manager, config)
        self.logger = logging.getLogger("rddr.proxy.incoming")
        self._setup_proxy(config['incoming-proxy'])

    def _setup_proxy(self, proxy_config: dict):
        self.port = proxy_config['port']
        super()._setup_proxy(proxy_config)

    def init_server(self):
        """
        Start an asyncio server for this proxy.
        Passes the _new_client member method as the new client callback.
        """
        self.server = RddrProxyServer(self.protocol.create_server(self.host, self.port), self._new_client_socket)
        self.logger.info(f"Bound to {self.host}:{self.port}")

    def _new_client_socket(self, client_sock):
        parent, child = Pipe()
        proc = Process(target=self._new_client_process, args=(child, client_sock, self.shared_state))
        proc.start()
        while not proc.pid:
            time.sleep(.25)
        reduction.send_handle(parent, client_sock.fileno(), proc.pid)
        client_sock.close()

    def _new_client_process(self, pipe, sock, shared_state):
        client_sock = socket.fromfd(reduction.recv_handle(pipe), socket.AF_INET, socket.SOCK_STREAM)
        self.shared_state = shared_state
        self.diff_adapter.shared_state = shared_state
        loop = asyncio.new_event_loop()
        client_sock = sock
        reader, writer = loop.run_until_complete(asyncio.open_connection(sock=client_sock, loop=loop))
        loop.run_until_complete(self._new_client(reader, writer))
        self.logger.debug("Loop done")

    async def _new_client(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        """
        Sets up two tunnels between the external client and all N application variants.
        One for each traffic direction.

        :param reader: StreamReader instance
        :param writer: StreamWriter instance
        """
        loop = asyncio.get_event_loop()
        try:
            self.logger.debug("New client on incoming proxy")
            dests_r = []
            dests_w = []
            for addr in self.config['addrlist']:
                host, port = addr.split(':')
                host = socket.gethostbyname(host)
                self.logger.debug(f"Connecting to {addr}...")
                r, w = await self.protocol.open_connection(host, port)
                self.logger.debug(f"Connected to {addr}")
                dests_r.append(r)
                dests_w.append(w)
            done, pending = await asyncio.wait(
                [self._tunnel([reader], dests_w),
                    self._tunnel(dests_r, [writer])],
                return_when=asyncio.FIRST_EXCEPTION
            )
            for result in done:
                if result.exception():
                    raise result.exception()
        except Exception as e:
            traceback.print_exc()
        finally:
            writer.close()
            await writer.wait_closed()
            for s in dests_w:
                s.close()
                await s.wait_closed()
            self.logger.debug("Closing tunnel")

    # async def run(self):
    #     """
    #     Serves forever.
    #     """
    #     with ThreadPoolExecutor(max_workers=1) as executor:
    #         executor.submit(self.server.serve_forever)
