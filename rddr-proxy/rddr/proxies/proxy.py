import select
import logging
import socket
from abc import ABC, abstractmethod
from typing import Dict, Type, TypeVar, Callable, List
import time
import asyncio
import traceback
from collections import OrderedDict
import signal
import os

from rddr.protocols import rddr_protocol_factory, RddrProtocol


class RddrProxy(ABC):
    """
    This is an abstract class, a parent to the incoming
    and outgoing proxies used by RDDR.

    Proxies are built on the asyncio library in Python 3.8.
    This framework was found to be faster and cleaner
    than the prior state machine-based implementations.

    :param config: Dictionary of user config provided to RDDR at the command line
    """
    def __init__(self, mp_manager, config: dict):
        self.config = config
        self.logger = logging.getLogger("rddr.proxy")
        self.read_len = 32*1024
        self.mp_manager = mp_manager
        self.shared_state = self.mp_manager.Namespace()
        # signal.signal(signal.SIGCHLD, self._reap_child_process)

    async def _new_client(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        """
        Callback function when a new client connects.
        Arguments are a StreamReader and StreamWriter instance
        associated with this connection.
        Complies with the interface documented by the asyncio library
        for the client_connected_cb passed to the asyncio.create_server function.

        Subclasses should override this function and make use of the _tunnel
        helper coroutine to configure the necessary tunnels.

        :param reader: StreamReader instance
        :param writer: StreamWriter instance
        """
        raise NotImplementedError

    def filter_pair(self):
        if self.config['filter']:
            return self.config['addrlist'][:2]
        else:
            return None

    def _get_diff_class(self, import_path):
        components = import_path.split('.')
        mod = __import__(components[0])
        for comp in components[1:]:
            mod = getattr(mod, comp)
        return mod

    def _setup_proxy(self, proxy_config: dict):
        """
        Derived classes will call this function
        and pass it the config relevant to this proxy
        from the main config file. Will automatically
        instantiate the correct class derived from
        rddr.protocols.protocol.RddrProtocol based
        on the config.

        :param proxy_config: Section of the global config file relevant to this proxy
        """
        host = proxy_config['host']
        self.host = host
        protocol = proxy_config['protocol']
        self.protocol = rddr_protocol_factory(protocol)
        self.enforcing = proxy_config['enforcing']
        diff_cls = self._get_diff_class(proxy_config['diff-class'])
        self.diff_adapter = diff_cls(self.mp_manager, self.shared_state, self.config['filter'], self.logger, proxy_config['diff-params'])
        self.timeout = proxy_config['timeout']

    def _reap_child_process(self, sig, frame):
        pid, exitcode = os.wait()
        print("Child process {pid} exited with code {exitcode}".format(
            pid=pid, exitcode=exitcode
        ))

    async def _tunnel(self, readers: List[asyncio.StreamReader], writers: List[asyncio.StreamWriter]):
        """
        Generic tunnel useful to incoming and outgoing proxy subclasses.
        No need to be overridden.
        The tunnel will read from arbitrarily many StreamReader instances
        in the readers list and diff their traffic using the diff module
        associated with this proxy.
        If the traffic is not divergent, it will be written to all
        StreamWriter instances in the writers list.

        :param readers: List of StreamReaders to read and diff.
        :param writers: List of StreamWriters who will be forwarded data sent by readers.
        """
        self.logger.debug("Opened tunnel")
        try:
            readers = [(self.protocol.get_stream_addr(r), r) for r in readers]
            writers = [(self.protocol.get_stream_addr(w), w) for w in writers]
            assert all([key is not None for key, _ in readers]), "Reader address is None"
            assert all([key is not None for key, _ in writers]), "Writer address is None"
            rdatas = [b""]*len(readers)
            bytes_read = [0 for raddr, _ in readers]
            readers_subset = None
            while True:
                all_readers_closed = True
                for reader_i, (raddr, reader) in enumerate(readers):
                    if readers_subset is None or reader in readers_subset:
                        self.logger.debug(f"Waiting to read from {raddr}")
                        d = await reader.read(self.read_len)
                        self.logger.debug(f"Got {len(d)} bytes of data from addr {raddr}")
                        if len(d) > 0:
                            all_readers_closed = False
                            rdatas[reader_i] += d
                            bytes_read[reader_i] += len(d)
                consistent = True
                if len(readers) > 1:
                    assert len(writers) == 1
                    diff_results = self.diff_adapter.diff_traffic(rdatas.copy())
                    bytes_to_write, need_more_bytes = zip(*diff_results)
                    consistent = all([b != -1 for b in bytes_to_write])
                    readers_subset = None
                    # We identify which readers have sent less data and read just from those
                    # on the next iteration. This avoids blocking on a socket which we have
                    # already read all of the data from.
                    if any(need_more_bytes):
                        self.logger.debug("Moving to reading from subset")
                        readers_subset = [reader for i, (raddr, reader) in enumerate(readers) if need_more_bytes[i]]
                        self.logger.debug([raddr for i, (raddr, reader) in enumerate(readers) if need_more_bytes[i]])
                else:
                    bytes_to_write = [len(rdatas[0])]*len(rdatas)

                if not consistent:
                    self.logger.warning("Not consistent")
                if not consistent and self.enforcing:
                    self.logger.warning("Withholding response")
                    for _, writer in writers:
                        writer.write(self.diff_adapter.render_denial())
                        await writer.drain()
                        writer.close()
                        await writer.wait_closed()
                    break
                else:
                    wdatas = [rdatas[0][:bytes_to_write[0]]]*len(writers)
                    if len(writers) > 1:
                        assert len(readers) == 1
                        wdatas = self.diff_adapter.modify_traffic(wdatas[0], len(writers))
                    for writer_i, (waddr, writer) in enumerate(writers):
                        if len(wdatas[writer_i]) > 0:
                            self.logger.debug(f"Sending {len(wdatas[writer_i])} bytes to {waddr}")
                            writer.write(wdatas[writer_i])
                            await writer.drain()
                # Wrote to writers, clear rdatas
                rdatas = [rdata[bytes_to_write[i]:] for i, rdata in enumerate(rdatas)]
                if all_readers_closed:
                    self.logger.debug("All readers closed")
                    break
        except Exception:
            traceback.print_exc()
        finally:
            self.logger.debug(f"Closing one-way: {len(readers)} reader(s) {len(writers)} writer(s)")
