import logging
import select
import socket
import asyncio
import traceback
from typing import Dict, List
from collections import OrderedDict
from multiprocessing import Process, reduction, Pipe

from .proxy import RddrProxy
from .proxy_server import RddrProxyServer


class RddrOutgoingProxy(RddrProxy):
    """
    Implements an outgoing proxy for RDDR.
    Merges outgoing requests from N applications
    to some other microservice and replicates the response back.

    :param config: Dictionary of user config provided to RDDR at the command line
    :param dest: Destination address where incoming requests will be forwarded.
                 String format: "<HOST>:<PORT>"
    """
    def __init__(self, mp_manager, config: dict, dest: str):
        super().__init__(mp_manager, config)
        self.logger = logging.getLogger(f"rddr.proxy.outgoing.{dest}")
        self.dest = dest
        self._setup_proxy(config['outgoing-proxies'][dest])
        self.port_queues = OrderedDict()

    def _setup_proxy(self, proxy_config: dict):
        self.ports = proxy_config['ports']
        super()._setup_proxy(proxy_config)

    def init_server(self):
        """
        Start an asyncio server for this proxy.
        Passes the _new_client member method as the new client callback.
        """
        self.servers = []
        for port in self.ports:
            self.servers.append(RddrProxyServer(self.protocol.create_server(self.host, port),
                self._gen_new_client_handler(port)))
            # Create a queue of conns for each port
            self.port_queues[port] = []
            self.logger.info(f"Bound to {self.host}:{port}")

    def _gen_new_client_handler(self, port):
        return lambda sock: self._new_client_socket(sock, port)

    def _new_client_socket(self, client_sock, port):
        self.logger.debug(f"New client on outgoing proxy (port {port})")
        self.port_queues[port].append(client_sock)
        all_ports_pending = all([len(q) > 0 for q in self.port_queues.values()])
        if all_ports_pending:
            clients = [q.pop(0) for q in self.port_queues.values()]
            parent, child = Pipe()
            proc = Process(target=self._new_client_process, args=(child, self.shared_state))
            proc.start()
            while not proc.pid:
                self.logger.debug("No child pid yet")
                time.sleep(.25)
            for client in clients:
                reduction.send_handle(parent, client.fileno(), proc.pid)
                client.close()

    def _new_client_process(self, pipe, shared_state):
        client_socks = []
        while pipe.poll():
            client_socks.append(socket.fromfd(reduction.recv_handle(pipe), socket.AF_INET, socket.SOCK_STREAM))
            self.logger.debug("Got socket")
        self.shared_state = shared_state
        loop = asyncio.new_event_loop()
        readers = []
        writers = []
        for client_sock in client_socks:
            reader, writer = loop.run_until_complete(asyncio.open_connection(sock=client_sock, loop=loop))
            readers.append(reader)
            writers.append(writer)
        loop.run_until_complete(self._new_client(readers, writers))
        # for client_sock in client_socks:
        #     client_sock.shutdown(socket.SHUT_RDWR)
        #     client_sock.close()

    async def _new_client(self, clients_r: List[asyncio.StreamReader], clients_w: List[asyncio.StreamWriter]):
        """
        Sets up two tunnels between the backend service and all N application variants.
        One for each traffic direction.

        :param clients_r: StreamReader instances
        :param clients_w: StreamWriter instances
        """
        try:
            dest_host, dest_port = self.dest.split(':')
            dest_host = socket.gethostbyname(dest_host)
            dest_r, dest_w = await self.protocol.open_connection(dest_host, dest_port)
            self.logger.debug("Connected to proxy destination")
            done, pending = await asyncio.wait(
                [self._tunnel([dest_r], clients_w), self._tunnel(clients_r, [dest_w])],
                return_when=asyncio.FIRST_EXCEPTION
            )
            for result in done:
                if result.exception():
                    raise result.exception()
        except:
            traceback.print_exc()
        finally:
            self.logger.debug("Closing tunnel")
            dest_w.close()
            await dest_w.wait_closed()
            for s in clients_w:
                s.close()
                await s.wait_closed()

    # async def run(self):
    #     """
    #     Serves forever.
    #     """
    #     with ThreadPoolExecutor(max_workers=len(self.servers)) as executor:
    #         for server in self.servers:
    #             executor.submit(server.serve_forever)
    #     # done, pending = await asyncio.wait(
    #     #     [server.serve_forever() for server in self.servers],
    #     #     return_when=asyncio.FIRST_EXCEPTION
    #     # )
    #     # for result in done:
    #     #     if result.exception():
    #     #         raise result.exception()
