import asyncio
import select

class RddrProxyServer(object):
    def __init__(self, srvsock, new_client_cb):
        self.sock = srvsock
        self.new_client_cb = new_client_cb

    def serve_forever(self):
        self.sock.listen(5)
        while True:
            client_sock, addr = self.sock.accept()
            self.new_client_cb(client_sock)
