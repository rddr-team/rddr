from rddr.protocols.protocol_ssl import RddrProtocolSsl
from rddr.protocols.protocol_tcp import RddrProtocolTcp
from rddr.protocols.protocol_ssh import RddrProtocolSsh

def rddr_protocol_factory(protocol: str):
    """
    Given a protocol specified as a string,
    create and return an instance of the
    appropriate subclass of
    :class:`RddrProtocol <rddr.protocols.protocol.RddrProtocol>`.
    Raises ValueError on unsupported protocol input.

    :param protocol: Supported protocols: tcp, ssh, ssl
    """
    if protocol == 'tcp':
        return RddrProtocolTcp()
    if protocol == 'ssh':
        return RddrProtocolSsh()
    if protocol == 'ssl':
        return RddrProtocolSsl()
    else:
        raise ValueError(f"Unsupported protocol: {protocol}")
