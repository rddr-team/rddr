import socket
import ssl
import logging
import asyncio
from asyncio import StreamReader, StreamWriter
from typing import Tuple, Callable

from rddr.protocols.protocol import RddrProtocol

class RddrSSLContext(ssl.SSLContext):
    pass


class RddrProtocolSsl(RddrProtocol):
    """
    Support for SSL on top of TCP
    
    :param cert: Path to the certificate file
    :param key: Path to the key file
    """
    def __init__(self, cert: str = "certs/clientcert.pem", key: str = "certs/clientkey.pem"):
        super().__init__()
        self.logger = logging.getLogger("rddr.protocol.ssl")
        self.logger.debug("ssl init")
        self.certfile = cert
        self.keyfile = key
        self.ssl_ctx = RddrSSLContext(ssl.PROTOCOL_TLS)
        self.ssl_ctx.load_cert_chain(certfile=self.certfile, keyfile=self.keyfile)

    async def open_connection(self, host: str, port: int) -> Tuple[StreamReader, StreamWriter]:
        """
        Creates a new socket and wraps it in
        an encrypted session for forwarding data.
        Returns (StreamReader, StreamWriter).

        :param host: Host to connect to
        :param port: Port to connect to
        """
        return await asyncio.open_connection(host, port, ssl=self.ssl_ctx)

    def create_server(self,
                      host: str,
                      port: int) -> None:
        """
        Coroutine. Creates a server socket with SSL context.

        :param host: Hostname/IP to bind server to
        :param port: Port to bind server to
        """
        srvsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        srvsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        srvsocket.bind((host, port))
        srvsocket.listen(5)
        return self.ssl_ctx.wrap_socket(srvsocket, server_side=True, do_handshake_on_connect=True)
