import socket
from asyncio import StreamReader, StreamWriter
from typing import Callable, Tuple

from rddr.protocols.protocol import RddrProtocol

class RddrProtocolSsh(RddrProtocol):

    async def open_connection(self, host: str, port: int) -> Tuple[StreamReader, StreamWriter]:
        """
        Not yet implemented.
        """
        raise NotImplementedError

    def create_server(self,
                      host: str,
                      port: int) -> None:
        """
        Not yet implemented.
        """
        raise NotImplementedError
