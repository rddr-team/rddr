from abc import ABC, abstractmethod
from asyncio import StreamReader, StreamWriter
from typing import Callable, Union, Tuple

class RddrProtocol(ABC):
    """
    Abstract class to be used as a base for
    concrete protocols.
    """
    def __init__(self):
        super().__init__()

    @abstractmethod
    async def open_connection(self, host: str, port: int) -> Tuple[StreamReader, StreamWriter]:
        """
        Abstract coroutine.
        Opens a connection to host:port.
        Returns (StreamReader, StreamWriter)

        :param host: Host to connect to
        :param port: Port to connect to
        """
        raise NotImplementedError

    @abstractmethod
    def create_server(self,
                            host: str,
                            port: int) -> None:
        """
        Coroutine. Creates a server socket.

        :param host: Hostname/IP to bind server to
        :param port: Port to bind server to
        """
        raise NotImplementedError

    def get_stream_addr(self, stream: Union[StreamReader, StreamWriter]) -> Union[str, None]:
        """
        Given an asyncio stream (either StreamReader or StreamWriter)
        returns a string representing the host and port of the party
        on the other end of the streams.
        Format: "<host>:<port>"
        May return None if address cannot be retrieved.

        :param stream: StreamReader or StreamWriter used to communicate with remote party
        """
        peername = stream._transport.get_extra_info('peername')
        if peername is None:
            return None
        else:
            host, port = peername
            return f"{host}:{port}"
