import socket
import asyncio
from asyncio import StreamReader, StreamWriter
from typing import Tuple, Callable

from rddr.protocols.protocol import RddrProtocol

class RddrProtocolTcp(RddrProtocol):
    """
    Support for unencrypted TCP.
    """
    async def open_connection(self, host: str, port: int) -> Tuple[StreamReader, StreamWriter]:
        """
        Creates a new unencrypted socket for forwarding data.
        Returns (StreamReader, StreamWriter).

        :param host: Host to connect to
        :param port: Port to connect to
        """
        return await asyncio.open_connection(host, port)

    def create_server(self,
                      host: str,
                      port: int) -> None:
        """
        Coroutine. Creates a server socket.

        :param host: Hostname/IP to bind server to
        :param port: Port to bind server to
        """
        srvsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        srvsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        srvsocket.bind((host, port))
        return srvsocket
