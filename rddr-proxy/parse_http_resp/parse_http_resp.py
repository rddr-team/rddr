from io import BytesIO
import logging
from logging import Logger
import gzip

# Fields to omit from the header so as to not diff them
omit_from_header = [
    b'Date', # Omit timestamp
]

def parse_http_response(respbytes: bytes,
                        logger: Logger = logging.getLogger("rddr.parse_http_resp")) -> bytes:
    """
    Massages an HTTP response prior to parsing.
    Uncompresses content if compressed with gzip.
    Removes Date header field.
    If compressed, also removes Content-Length field.
    Returns the entire HTTP response, both status, headers, and content, as a bytestring.

    :param respbytes: The HTTP response directly from the server.
    :param logger: Logger for printing messages.
    """
    if b'\r\n\r\n' in respbytes:
        headers, sep, content = respbytes.partition(b'\r\n\r\n')
        header_dict = {}
        status, _, header_fields = headers.partition(b'\r\n') # Ignore status code
        for h in header_fields.splitlines():
            key, _, val = h.partition(b':')
            header_dict[key.strip()] = val.strip()
        if header_dict.get(b'Content-Encoding', b'')==b'gzip':
            content = gzip.decompress(content)
            # Content length when compressed refers to compressed length. Ignore.
            omit_from_header.append(b'Content-Length')
        for field in omit_from_header:
            header_dict.pop(field, None)
        # Reconstruct header with just what we care about
        headers = status + b'\r\n' + b'\r\n'.join([k + b': ' + v for k,v in header_dict.items()])

    else:
        headers = sep = b''
        content = respbytes

    return headers + sep + content
