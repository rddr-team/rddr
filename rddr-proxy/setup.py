from distutils.core import setup

version = '0.1'

setup(name='rddr',
      version=version,
      description="RDDR Proxy: N versioning proxy for microservices",
      author="Riley Wood and Tony Espinoza",
      author_email="riley.wood@utexas.edu, am.espinoza@utexas.edu",
      include_package_data=True,
      packages=[
            'rddr',
            'rddr.proxies',
            'rddr.protocols',
            'parse_http_resp',
            'rddr_diff_builtins',
      ],
      install_requires=[
            "pyyaml",
            "cerberus",
            "jsondiff",
      ])
