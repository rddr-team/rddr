TODO
- Add Envoy to Kubernetes deployment
- Set up automated testing
    - Negative testing, make sure we can catch real violations (some dummy app)
- Set up distributed DVWA with RDDR in Kubernetes
- Set up GitLab with RDDR
	- N-Version particular microservices within GitLab deployment
- Add notes to future work section of paper mentioning how you might extend RDDR to stateful microservices
- Create reverse RDDR (ROQ)
    - Bind to multiple interfaces to intercept all outgoing requests from instances. Will merge outgoing requests and notify on divergence.
	- Create test setup where N-versioned instanced make outgoing request to 1+ local microservices.
- Use "content type" header info to select appropriate diff lib

DONE
- Made rddr use the actual de-noise pair
	- It currently just uses the first two by default (Should we keep it this way?)
- Finish OWASP investigation and writeup (Riley)
- Work on plugin API to allow users to plug in their own diff implementation (Tony)
	- Move existing diff code to plugin API. All differs should use this API (Tony)
- Add permissive/enforcing option to config (Riley)
- Set up the system in a containerized environment
	- Maybe do docker first, followed by k8s
- Devise a way to force "real" divergent behavior in the flask server instances (Riley)
  - This is done by injecting a "secret" file via configmap which differs in just one of the instances.
- Fix deployment so that prints from rddr show up in k8s logs (riley)
- Update RDDR to actually use the de-noising pair, do de-noising
