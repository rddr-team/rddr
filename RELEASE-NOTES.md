Release Notes
-------------

# Next release
- Update documentation

# v0.6.0
- Add MIT LICENSE
- Add ASLR demo. Change flow control to support it.
- Fix Postgres plugin hang
- Add Postgres/CockroachDB demo.

# v0.5.1
- Fixing postgres bug when receiving lots of traffic (eg many rows)

# v0.5.0
- Fix docs
- Add built-in postgres diff module
- Moved to object-oriented diff module interface

# v0.4.0
- Rearchitect outgoing proxy. Use one port per instance to differentiate them.

# v0.3.0
- Diff entire HTTP header including cookies
- Normalize DVWA cookies between variants
- Added automated tests for NGINX vuln CVE-2017-7529
- Fix exception when filtering HTTP

# v0.2.1
- Add rddr\_byte\_diff to packages list

# v0.2.0
- Fixes spurious failures on DVWA deployment test

# v0.1.0
- First release of RDDR
- Working deployments:
  - Flask server
- Deployments with some known issues:
  - DVWA

