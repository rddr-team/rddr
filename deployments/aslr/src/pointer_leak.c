#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/personality.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa//inet.h>

static void print_input(int sock, char *input, int input_len);
static void gadget(void);

int main(int argc, char *argv[])
{
    if(argc != 3) {
        printf("Supply two positional arguments (host and port)\n");
        return 1;
    }
    printf("gadget at %p\n", gadget);
    printf("Binding to %s:%s...\n", argv[1], argv[2]);
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(atoi(argv[2]));
    int address_len = sizeof(address);
    printf("port: %d\n", atoi(argv[2]));
    inet_aton(argv[1], &address.sin_addr);
    
    int server_sock = socket(AF_INET, SOCK_STREAM, 0);
    perror("socket");
    int opt = 1;
    setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR|SO_REUSEPORT, &opt, sizeof(opt));
    perror("setsockopt");
    bind(server_sock, (struct sockaddr*) &address, sizeof(address));
    perror("bind");
    listen(server_sock, 10);
    perror("listen");
    while(1) {
        int client_sock = accept(server_sock, (struct sockaddr *)&address, (socklen_t*)&address_len);
        char client_data[256];
        int n = read(client_sock, client_data, sizeof(client_data));
        client_data[n] = 0;
        print_input(client_sock, client_data, n);
        fsync(client_sock);
        shutdown(client_sock, SHUT_RDWR);
    }
    printf("Leaving main\n");
    return 0;
}

char outbuf[256];
static void print_input(int sock, char *input, int input_len)
{
    void (*volatile gadget_ptr)(void) = gadget;
    char buf[16];
    memset(buf, 0, sizeof(buf));
    memcpy(buf, input, input_len);
    snprintf(outbuf, sizeof(outbuf), "buf (len %lu): %s\n", strlen(buf), buf);
    write(sock, outbuf, strnlen(outbuf, sizeof(outbuf)+1));
    perror("write socket");
}

static void gadget(void)
{
    printf("Entered gadget!!!\n");
    while(1);
    return;
}
