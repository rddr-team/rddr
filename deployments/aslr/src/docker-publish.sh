#!/bin/bash

docker build -t aslr:latest .
docker tag aslr localhost:32000/aslr
docker push localhost:32000/aslr

docker tag aslr utspark/aslr
docker push utspark/aslr
