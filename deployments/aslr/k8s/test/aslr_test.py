import re
import time
import socket
import pytest


@pytest.fixture(scope="session")
def url(request):
    return request.config.option.url

def test_consistent(url):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        host, port = url.split(':')
        port = int(port)
        s.connect((host, port))
        s.sendall(b'123456789ABCDEF')
        resp = s.recv(1024)
        assert len(resp) > 0

def test_inconsistent(url):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.settimeout(10)
        host, port = url.split(':')
        port = int(port)
        s.connect((host, port))
        s.sendall(b'123456789ABCDEFFFFFFFFFF')
        resp = s.recv(128)
        gadget_addr = resp[38:-1]
        # RDDR should prevent address leak
        assert len(resp) == 0
        assert len(gadget_addr) == 0
