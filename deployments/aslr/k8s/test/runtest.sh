#!/bin/bash
set -e

microk8s.kubectl apply -f ..

while [[ $(microk8s.kubectl get pods | awk '{ print $3 }' | tail -n +2 | grep -v Running) != "" ]]; do echo "Waiting for pods" && sleep 5; done

tox;

microk8s.kubectl delete -f ..

sleep 10

set +e


