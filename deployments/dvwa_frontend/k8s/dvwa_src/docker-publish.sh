#!/bin/bash

docker build -t dvwa_frontend:latest .

# Send to Kubernetes registry
docker tag dvwa_frontend localhost:32000/dvwa_frontend
docker push localhost:32000/dvwa_frontend

docker tag mysql:8.0 localhost:32000/mysql
docker push localhost:32000/mysql

# Send to Docker Hub
docker tag dvwa_frontend utspark/dvwa_frontend
docker push utspark/dvwa_frontend

