<?php

# If you are having problems connecting to the MySQL database and all of the variables below are correct
# try changing the 'db_server' variable from localhost to 127.0.0.1. Fixes a problem due to sockets.
#   Thanks to @digininja for the fix.

# Database management system to use
$DBMS = 'MySQL';
#$DBMS = 'PGSQL'; // Currently disabled

# Database variables
#   WARNING: The database specified under db_database WILL BE ENTIRELY DELETED during setup.
#   Please use a database dedicated to DVWA.
#
# If you are using MariaDB then you cannot use root, you must use create a dedicated DVWA user.
#   See README.md for more information on this.
$_DVWA = array();
$sqlhost_file = fopen("/mysql_host.txt", "r") or die("Unable to open mysql_host.txt!");
$sqlhost = trim(fread($sqlhost_file, filesize("/mysql_host.txt")));
$sqlport_file = fopen("/mysql_port.txt", "r") or die("Unable to open mysql_port.txt!");
$sqlport = trim(fread($sqlport_file, filesize("/mysql_port.txt")));
$_DVWA[ 'db_server' ]   = $sqlhost;
$_DVWA[ 'db_database' ] = 'dvwa';
$_DVWA[ 'db_user' ]     = 'root';
$_DVWA[ 'db_password' ] = 'p@ssw0rd';

# Used for postgres and mysql
$_DVWA[ 'db_port' ] = (int)$sqlport;

# ReCAPTCHA settings
#   Used for the 'Insecure CAPTCHA' module
#   You'll need to generate your own keys at: https://www.google.com/recaptcha/admin/create
$_DVWA[ 'recaptcha_public_key' ]  = '';
$_DVWA[ 'recaptcha_private_key' ] = '';

# Default security level
#   Default value for the secuirty level with each session.
#   The default is 'impossible'. You may wish to set this to either 'low', 'medium', 'high' or impossible'.
$seclvl_file = fopen("/default_seclvl.txt", "r") or die("Unable to open default_seclvl.txt!");
$seclvl = trim(fread($seclvl_file, filesize("/default_seclvl.txt")));
$_DVWA[ 'default_security_level' ] = $seclvl;

# Default PHPIDS status
#   PHPIDS status with each session.
#   The default is 'disabled'. You can set this to be either 'enabled' or 'disabled'.
$_DVWA[ 'default_phpids_level' ] = 'disabled';

# Verbose PHPIDS messages
#   Enabling this will show why the WAF blocked the request on the blocked request.
#   The default is 'disabled'. You can set this to be either 'true' or 'false'.
$_DVWA[ 'default_phpids_verbose' ] = 'false';

?>
