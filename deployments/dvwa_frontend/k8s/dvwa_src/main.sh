#!/bin/bash

echo $DVWA_DEFAULT_SECLVL > /default_seclvl.txt
echo $DVWA_MYSQL_HOST > /mysql_host.txt
echo $DVWA_MYSQL_PORT > /mysql_port.txt

echo '[+] Starting apache'
service apache2 start

while true
do
    tail -f /var/log/apache2/*.log
    exit 0
done
