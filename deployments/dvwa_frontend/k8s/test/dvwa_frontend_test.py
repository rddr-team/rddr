import requests
import re
import time
import pytest

@pytest.fixture(scope="session")
def url(request):
    return request.config.option.url

def extract_token(html):
    usertok_re = re.compile(r"<input type='hidden' name='user_token' value='([a-z0-9]+)' \/>")
    token = usertok_re.search(html)
    assert token
    return token.groups(0)

def test_consistent(url):
    r = requests.get(f'http://{url}/login.php', timeout=20)
    assert r.status_code==200

def test_inconsistent(url):
    sess = requests.Session()

    # Get login page
    r = sess.get(f'http://{url}/login.php', timeout=20)
    assert r.status_code==200

    token = extract_token(r.text)

    # Log in as admin
    r = sess.post(f'http://{url}/login.php',
                  data={
                      'Login': 'Login',
                      'username': 'admin',
                      'password': 'password',
                      'user_token': token},
                  timeout=20)
    assert r.status_code==200

    # Get setup page
    r = sess.get(f'http://{url}/setup.php', timeout=20)
    assert r.status_code==200

    token = extract_token(r.text)

    # Setup the database
    r = sess.post(f'http://{url}/setup.php#',
                  data={'create_db': 'Create / Reset Database', 'user_token': token},
                  timeout=20)
    assert r.status_code==200

    # Get login page
    r = sess.get(f'http://{url}/login.php', timeout=20)
    assert r.status_code==200

    token = extract_token(r.text)

    # Log in as admin
    r = sess.post(f'http://{url}/login.php',
                  data={
                      'Login': 'Login',
                      'username': 'admin',
                      'password': 'password',
                      'user_token': token},
                  timeout=20)
    assert r.status_code==200

    # Get SQL injection page
    r = sess.get(f'http://{url}/vulnerabilities/sqli/', timeout=20)
    assert r.status_code==200

    token = extract_token(r.text)

    # Then attempt SQL injection
    r = sess.get(f'http://{url}/vulnerabilities/sqli/?id=%27&Submit=Submit&user_token={token}#',
                 timeout=20)
    assert r.status_code==500

