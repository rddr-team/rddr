#!/bin/bash

clients=(8 16 32 64 128 256)

for c in ${clients[@]}; do
        pgbench --host $1 --port $2 --user postgres -c $c -j $c -t 10000 -S postgres | tee ${3}_pgbench_${c}clients.log
done

cat ${3}_pgbench_{8,16,32,64,128,256}clients.log | grep tps | grep excluding | awk '{print $3}' > ${3}_throughput.txt
cat ${3}_pgbench_{8,16,32,64,128,256}clients.log | grep latency | awk '{print $4}' > ${3}_latency.txt
rm  ${3}_pgbench_{8,16,32,64,128,256}clients.log


