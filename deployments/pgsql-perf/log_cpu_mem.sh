
cpu_log=$1_cpu.log
mem_log=$1_mem.log

touch $cpu_log;
touch $mem_log;
while true; do
	mpstat 1 1 | grep Average | awk '{print $12}' | tee -a $cpu_log;
	top -b -n 1 | grep "KiB Mem" | awk '{print $7}' | tee -a $mem_log;
done

