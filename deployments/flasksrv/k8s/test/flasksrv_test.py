import requests
import json
import pytest

@pytest.fixture(scope="session")
def url(request):
    return request.config.option.url

def test_consistent(url):
    r = requests.get(f'https://{url}/',
                     verify=False,
                     timeout=10)
    assert r.status_code==200
    json.loads(r.text)

def test_inconsistent(url):
    r = requests.get(f'https://{url}/secret',
                     verify=False,
                     timeout=10)
    assert r.status_code==500

