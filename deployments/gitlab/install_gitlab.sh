#!/bin/bash

##### READ ME #####
# Some lines of this script have been commented out,
# like those which install tools like kubectl and minikube.
# During your first run of the script, you should uncomment
# these lines so that you get all the necessary prerequisites.
# Afterwards, you may comment these lines again to expedite
# the setup process.

# Create local bin folder and add it to PATH
BIN=$HOME/bin
mkdir -p $BIN
export PATH=$PATH:$BIN
echo "Add $HOME/bin to your rc file for future use of kubectl, helm and minikube"

# Install Kubectl, Minikube and Helm
#curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl
#chmod +x kubectl
#mv kubectl $BIN
#curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
#chmod +x minikube
#install minikube $BIN

#install jq
#wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
#chmod +x jq-linux64
#mv jq-linux64 $BIN/jq

echo "Starting new VM with 4 CPU and 8GB Memory"
read -p "You can change settings here or continue with default. Do you wish to change?[y/n]" change
if [ $change == 'y' ]; then
  read -p "Memory: " mem
  read -p "vCPU: " cpu
  read -p "VM name: " name
else
  mem=8192
  cpu=4
  name="minikube"
fi
echo "Starting $name VM with $cpu CPUs and $mem Memory"
minikube --memory $mem --cpus $cpu start -p $name --vm-driver=virtualbox
echo "Check minikube running status"
minikube status -p $name
echo "Check kubectl install is proper"
kubectl version

IP=$(minikube ip -p $name)

#curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
#chmod +x get_helm.sh
#sed -i 's/\/usr\/local\/bin/$HOME\/bin/g' get_helm.sh
#bash get_helm.sh -v v2.16.1 --no-sudo
echo "Install Tiller in the minikube cluster"
helm init

# Set up secret containing password for external postgres
kubectl create secret generic gitlab-ext-postgresql-password --from-literal=pass='password'

# Expose external postgres database running on host to cluster
kubectl apply -f ./expose-host-postgres.yaml

# Gitlab download and edit configs
helm repo add gitlab https://charts.gitlab.io/
helm repo update
#helm fetch gitlab/gitlab
#tar xvf gitlab-*.tgz

cd gitlab
echo "Changed to gitlab directory"
#echo "Updating depricated apiVersion strings as per in https://kubernetes.io/blog/2019/07/18/api-deprecations-in-1-16/"
#echo "Replace \"extensions/v1beta1\" in Deployments and DaemonSets to \"apps/v1\" in the following files"
#files=$(grep -rin --include="*deployment.yaml" --include="*daemonset.yaml" "extensions/v1beta1" .)
#IFS=":|$IFS"
#f=(${files[@]})
#for i in ${!f[@]}; do
  #if [[ ${f[$i]} == *".yaml" ]]; then
    #echo "${f[$i]}"
    #sed -i 's/extensions\/v1beta1/apps\/v1/g' ${f[$i]}
  #fi
#done
#echo "Replace \"extensions/v1beta1\" in Pod security Policy to \"policy/v1beta1\" in the following files"
#files=$(grep -rin --include="*podsecuritypolicy.yaml" "extensions/v1beta1" .)
#f=(${files[@]})
#for i in ${!f[@]}; do
  #if [[ ${f[$i]} == *".yaml" ]]; then
    #echo "${f[$i]}"
    #sed -i 's/extensions/policy/g' ${f[$i]}
  #fi
#done

#echo "Configuring k8s Cluster IP in values.yaml"
#sed -i "s/externalIP.*/externalIP: $IP/g" values.yaml
#echo "Configuring dummy email for Let's Encrypt ACME Issuer in values.yaml"
#sed -i "s/# certmanager-issuer:/certmanager-issuer:/g" values.yaml
#read -p "Email: " email
#sed -i "s/# email: email@example.com/email: $email/g" values.yaml

#sed -i "s/http: \"\"/http: 32080/g" charts/nginx-ingress/values.yaml
#sed -i "s/https: \"\"/https: 32443/g" charts/nginx-ingress/values.yaml
#sed -i '${/"NodePort"/d;}' charts/nginx-ingress/templates/controller-service.yaml
#sed -i '/nodePorts/{n;d}' charts/nginx-ingress/templates/controller-service.yaml
#sed -i 's/.*targetPort: gitlab-shell.*/&\n& edit/' charts/nginx-ingress/templates/controller-service.yaml
#sed -i 's/targetPort: gitlab-shell edit/nodePort: 32022/g' charts/nginx-ingress/templates/controller-service.yaml


while [[ $(kubectl get pods --all-namespaces | awk '{ print $3 }' | tail -n +2 | grep -v 1/1) != "" ]]; do echo "Waiting for pods" && sleep 5; done

sleep 5

# Deploy gitlab
helm upgrade --install gitlab ./ --timeout 600 --set certmanager.install=false --set global.ingress.configureCertmanager=false --set gitlab-runjner.install=false --timeout 600

# add DNS in coredns using this guide https://coredns.io/2017/05/08/custom-dns-entries-for-kubernetes/
data="$(kubectl get cm coredns -n kube-system -o json | jq .data.Corefile)"
add='\'"$(echo $data | sed -n 's/\(.*\)\(n.*ready\)\(.*\)/\2/p')"
add="$(echo $add | sed -n 's/ready/rewrite name gitlab.example.com gitlab-nginx-ingress-controller.default.svc.cluster.local/p')"
newdata="$(echo $data | sed -n "s/\(.*\)\(n.*ready\)\(.*\)/\1\2\\$add\3/p")"
kubectl patch cm coredns -n kube-system --type=json -p="[{\"op\":\"replace\", \"path\": \"/data/Corefile\", \"value\":$newdata}]"
pods=$(kubectl get po -n kube-system -o json | jq .items[].metadata.name | grep coredns)
for pod in $pods; do
  len=${#pod}
  echo ${pod:1:len-2}
  kubectl delete pod ${pod:1:len-2} -n kube-system --force --grace-period=0
done

#echo "We will need sudo to modify /etc/hosts to point it to the VM"
#echo "$IP gitlab.example.com" | sudo tee -a /etc/hosts
git config --global http.sslverify false
echo "git clone commands will have gitlab.example.com:32443 for https and gitlab.example.com:32022 for ssh instead of just gitlab.example.com"

password=$(kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo)
echo "Login with root/$password"
