#!/bin/bash
  
SSHPRIVKEY=/etc/ssh/ssh_host_rsa_key
SSHPUBKEY=/etc/ssh/ssh_host_rsa_key.pub

TMPPRIVKEY=/tmp/ssh_host_rsa_key.nopass.pem

LSHPRIVKEY=/etc/lsh_host_key
LSHPUBKEY=/etc/lsh_host_key.pub


# Convert private key
openssl rsa -inform PEM -outform PEM -in ${SSHPRIVKEY} -out ${TMPPRIVKEY}

cat ${TMPPRIVKEY} | pkcs1-conv | \
sexp-conv -s advanced | \
sexp-conv -s canonical | lsh-writekey -l OPENSSHKEY --server


# Convert public key
ssh-conv < ${SSHPUBKEY} > ${LSHPUBKEY}

